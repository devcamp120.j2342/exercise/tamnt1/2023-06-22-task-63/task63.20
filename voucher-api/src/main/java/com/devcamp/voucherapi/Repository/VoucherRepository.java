package com.devcamp.voucherapi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.voucherapi.Models.Voucher;

public interface VoucherRepository extends JpaRepository<Voucher, Long> {

}
